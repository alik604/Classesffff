# MATH232
## Applied Linear Algebra MATH 232 

> Linear equations, matrices, determinants. Introduction to vector spaces and linear transformations and bases. Complex numbers. Eigenvalues and eigenvectors; diagonalization. Inner products and orthogonality; least squares problems. An emphasis on applications involving matrix and vector calculations. 


##### Final grade: B-
##### Class avg: C+ 
