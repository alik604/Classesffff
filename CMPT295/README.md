# CMPT295


## Topics
* Machine language programs
* Representation of symbolic and numeric data
* Representation of instructions (instruction set architecture)
* Machine code optimization
* Basic digital systems
* CPU organization
* Memory organization
* Timing attacks (time permitting)
